const express = require('express')
const app = express()
const server = require('http').Server(app)
const axios = require('axios')
const cors = require('cors')
const bodyParser = require('body-parser')


//add other middleware
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

const apiUrl = 'https://readthedocs.org/api/v3/'

app.post('/doc', async (req, res) => {

	axios({
        url: "projects/",
        method: "post",
        baseURL: apiUrl,
        headers: {
		   'Authorization': 'Token e36ec9ac09df126113ff55dc04bc98e8b0363313'
		},
        data: {
        	"name": "Totogil",
		    "repository": {
		        "url": "https://gitlab.com/GDansou/tracer-documentation.git",
		        "type": "git"
		    },
		    "homepage": "https://gitlab.com/GDansou/tracer-documentation.git",
		    "programming_language": "js",
		    "language": "en"
        }
    }).then(
	    (response)=>{
	    	axios({
		        url: "projects/totogil/versions/latest/builds/",
		        method: "post",
		        baseURL: apiUrl,
		        headers: {
				   'Authorization': 'Token e36ec9ac09df126113ff55dc04bc98e8b0363313'
				}
		    }).then(
			    (response)=>{
			    	console.log(response)
			    	res.send('Success build')
			    },
			    (error) => { 
			    	res.send(error)
			    }
		    )
	    },
	    (error) => { 
	    	res.send(error)
	    }
    )
                                                                  
})


server.listen(5000	, function() {
	console.log('Server started at ' + (new Date().toLocaleString().substr(10, 12)))
})
